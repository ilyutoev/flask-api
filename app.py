from flask import Flask
from flask_restful import Api
from flasgger import Swagger

from resources.product import ProductList, Product
from db import init_db


def create_app():
    """
    Entry point to the Flask RESTful Server application.
    """
    app = Flask(__name__)
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///data.db'
    api = Api(app)
    swagger = Swagger(app)

    api.add_resource(ProductList, '/product')
    api.add_resource(Product, '/product/<int:pk>')

    init_db(app)

    return app


if __name__ == '__main__':
    app = create_app()
    app.run(port=8888, debug=True)
