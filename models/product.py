from db import db


class ProductModel(db.Model):
    """Модель продукта"""
    __tablename__ = 'products'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100))
    sku = db.Column(db.String(50), index=True)
    group_id = db.Column(db.Integer, index=True)
    stock = db.Column(db.Integer)

    def __init__(self, title, sku, group_id, stock):
        self.title = title
        self.sku = sku
        self.group_id = group_id
        self.stock = stock

    def json(self):
        """Преобразуем данные о товаре в словарь."""
        return {'id': self.id, 'title': self.title, 'sku': self.sku, 'group_id': self.group_id, 'stock': self.stock}

    @classmethod
    def find_by_id(cls, _id):
        """Поиск товара по id."""
        return cls.query.filter_by(id=_id).first()

    @classmethod
    def find_by_sku(cls, sku):
        """Посик товара по sku."""
        return cls.query.filter_by(sku=sku).first()

    def save_to_db(self):
        """Сохранение товара."""
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        """Удаление товара."""
        db.session.delete(self)
        db.session.commit()
