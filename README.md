# Тестовое api на Flask


## Запуск в Docker 

```bash
docker build -t flask-api:latest .

docker run -d -p 8888:8888 flask-api
```

## Как установить и запустить локально

Для работы бота нужен Python версии не ниже 3.6.

```bash
pip install -r requirements.txt

python app.py
```

# Запуск тестов

```bash
pytest -v
```

# Swagger

Документация к апи находится по пути `/apidocs/`
