from flask_restful import Resource, reqparse

from models.product import ProductModel


class Product(Resource):
    """API для получения еденичного товара, удаления и изменения остатков."""
    def get(self, pk):
        """
        Получение товара по его id
        ---
        responses:
            200:
                description: A single product item
                schema:
                    id: Product
                    properties:
                        id:
                            type: integer
                            description: Product id
                        title:
                            type: string
                            description: Product title
                        sku:
                            type: string
                            description: Product unique sku
                        group_id:
                            type: integer
                            description: Product group id
                        stock:
                            type: integer
                            description: Product stock count
        """
        product = ProductModel.find_by_id(pk)
        if product:
            return product.json()
        return {'message': 'Product not found'}, 404

    def delete(self, pk):
        """
        Удаление товара по его id
        ---
        responses:
            200:
                description: Successfully product delete
                schema:
                    id: Product
                    properties:
                        message:
                            type: string
                            description: Delete status
            404:
                description: Product not found
                schema:
                    id: Product
                    properties:
                        message:
                            type: string
                            description: Delete status
        """
        product = ProductModel.find_by_id(pk)
        if product:
            return product.delete_from_db()
        return {'message': 'Product not found'}, 404

    def patch(self, pk):
        """
        Обновление остатков у товара по его id
        ---
        parameters:
            - in: path
              name: stock
              type: integer
              required: true
        responses:
            200:
                description: Successfully product update
                schema:
                    id: Product
                    properties:
                        message:
                            type: string
                            description: Update status
            404:
                description: Product not found
                schema:
                    id: Product
                    properties:
                        message:
                            type: string
                            description: Update status
            400:
                description: Result stock less than 0
                schema:
                    id: Product
                    properties:
                        message:
                            type: string
                            description: Update status
        """
        product = ProductModel.find_by_id(pk)
        if not product:
            return {'message': 'Product not found'}, 404

        parser = reqparse.RequestParser()
        parser.add_argument('stock', type=int, required=True)
        data = parser.parse_args()

        stock = data['stock']
        resulted_stock = product.stock + stock
        if resulted_stock < 0:
            return {'message': 'Products result stock less than 0'}, 400

        product.stock = resulted_stock
        product.save_to_db()
        return product.json()


class ProductList(Resource):
    """API для получения спсика товаров и создания нового."""

    def get(self):
        """
        Список товаров
        ---
        parameters:
            - in: path
              name: group_id
              type: integer
              required: false
        responses:
            200:
                description: Product list
                schema:
                    type: array
                    items:
                        properties:
                            id:
                                type: integer
                                description: Product id
                            title:
                                type: string
                                description: Product title
                            sku:
                                type: string
                                description: Product unique sku
                            group_id:
                                type: integer
                                description: Product group id
                            stock:
                                type: integer
                                description: Product stock count
        """
        parser = reqparse.RequestParser()
        parser.add_argument('group_id', type=int)
        filters = parser.parse_args()

        if not filters['group_id']:
            filters.pop('group_id')

        return [product.json() for product in ProductModel.query.filter_by(**filters)]

    def post(self):
        """
        Создаем товар
        ---
        parameters:
            - in: body
              required: true
              schema:
                properties:
                    title:
                        type: string
                        description: Product title
                    sku:
                        type: string
                        description: Product unique sku
                    group_id:
                        type: integer
                        description: Product group id
                    stock:
                        type: integer
                        description: Product stock count
        responses:
            200:
                description: Product create
                schema:
                    properties:
                        message:
                            type: string
                            description: Post status
            400:
                description: Product already exists
                schema:
                    properties:
                        message:
                            type: string
                            description: Post status
        """

        parser = reqparse.RequestParser()
        parser.add_argument('title', type=str, required=True)
        parser.add_argument('sku', type=str, required=True)
        parser.add_argument('group_id', type=int, required=True)
        parser.add_argument('stock', type=int, required=True)
        data = parser.parse_args()

        if ProductModel.find_by_sku(data['sku']):
            return {'message': 'Product with that sku already exists.'}, 400

        product = ProductModel(**data)
        product.save_to_db()
        return {'message': 'Product created successfully.'}, 201
