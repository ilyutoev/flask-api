FROM        python:3.8

RUN         mkdir -p /opt/app

COPY        . /opt/app
WORKDIR     /opt/app
RUN         pip install --no-cache-dir -r /opt/app/requirements.txt

ENTRYPOINT  ["python"]
CMD         ["app.py"]