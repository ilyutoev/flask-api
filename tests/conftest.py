import os

import pytest

from app import create_app
from db import db as _db


TEST_DATABASE_URI = os.getenv('TEST_DATABASE_URI', 'postgresql+psycopg2://tester:12345@db/flaskdb_test')


@pytest.fixture(scope='session')
def app(request):
    """ Session wide test 'Flask' application """

    app = create_app()
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///data_test.db'
    ctx = app.app_context()
    ctx.push()

    yield app

    ctx.pop()


@pytest.fixture
def client(app):
    return app.test_client()


@pytest.fixture(scope='session')
def db(app, request):
    """ Session-wide test database """
    _db.drop_all()
    _db.app = app
    _db.create_all()

    yield _db

    _db.drop_all()

#
# @pytest.fixture(scope='function')
# def session(db, request):
#     """ Creates a new database session for a test """
#     connection = db.engine.connect()
#     transaction = connection.begin()
#
#     options = dict(bind=connection, binds={})
#     session = db.create_scoped_session(options=options)
#
#     db.session = session
#     yield db.session
#
#     transaction.rollback()
#     connection.close()
#     session.remove()


# import pytest
#
# from app import create_app
# from db import db
#
#
# def new_app():
#     """Возвращаем Flask объект с тестовой бд"""
#     app = create_app()
#     app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///data_test.db'
#     return app
#
#
# def set_up():
#     """Создае таблицы в бд."""
#     db.create_all()
#     db.session.commit()
#
#
# def tear_down():
#     """Удаляем таблцы в бд."""
#     db.session.remove()
#     db.drop_all()
#
#
# @pytest.fixture
# def test_client():
#     """Создаем тестовый клиент приложения"""
#     app = create_app()
#     client = app.test_client()
#     dir(app)
#     set_up()
#     yield client
#     tear_down()
