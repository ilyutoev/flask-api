import json


def test_create_product(client, db):
    data = {
        "title": "test",
        "sku": "1",
        "group_id": 2,
        "stock": 10
    }
    response = client.post("/product", data=data)
    response_data = json.loads(response.data)
    result = {"message": "Product created successfully."}
    assert response.status_code == 201
    assert response_data == result

    response = client.post("/product", data=data)
    response_data = json.loads(response.data)
    result = {"message": "Product with that sku already exists."}
    assert response.status_code == 400
    assert response_data == result


def test_get_product(client, db):
    response = client.get("/product/1")
    result = {
        "id": 1,
        "title": "test",
        "sku": "1",
        "group_id": 2,
        "stock": 10
    }
    assert response.status_code == 200
    assert json.loads(response.data) == result

    response = client.get("/product/10000")
    assert response.status_code == 404


def test_patch_product(client, db):
    data = {"stock": -9}
    response = client.patch("/product/1", data=data)
    product = json.loads(response.data)
    waited_stock = 1
    assert response.status_code == 200
    assert product.get("stock") == waited_stock

    data = {"stock": 3}
    response = client.patch("/product/1", data=data)
    product = json.loads(response.data)
    waited_stock = 4
    assert response.status_code == 200
    assert product.get("stock") == waited_stock

    data = {"stock": -100}
    response = client.patch("/product/1", data=data)
    assert response.status_code == 400

    response = client.patch("/product/1000000")
    assert response.status_code == 404


def test_delete_product(client, db):
    response = client.delete("/product/1")
    assert response.status_code == 200

    response = client.delete("/product/1")
    assert response.status_code == 404
    assert json.loads(response.data) == {"message": "Product not found"}


def test_get_products(client):
    data = {
        "title": "test",
        "sku": "1",
        "group_id": 2,
        "stock": 10
    }
    response = client.post("/product", data=data)
    assert response.status_code == 201

    data = {
        "title": "test",
        "sku": "2",
        "group_id": 1,
        "stock": 10
    }
    response = client.post("/product", data=data)
    assert response.status_code == 201

    response = client.get("/product")
    response_data = json.loads(response.data)
    assert response.status_code == 200
    assert len(response_data) == 2

    response = client.get("/product?group_id=1")
    response_data = json.loads(response.data)
    assert response.status_code == 200
    assert len(response_data) == 1
